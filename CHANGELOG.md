# Change Log


## Version Compatibility

| Oxygen Version    | Package Version   | Branch           |
| ----------------- | ----------------- | ---------------- |
| v6 / Laravel v9   | 2.1               | master           |
| v5 / Laravel v8   | 2.0               | 2.x              |
| v4 / Laravel v7   | 1.x               | version/v1.x     |

## v2.1
- Support for Oxygen v6 / Laravel v9
- 

## v2.x
- Support for Oxygen v5 / Laravel v8
- Renamed `devurl` to `dev_url`
- Removed dependency on Laravel Installer, because it becomes unstable

## v1.0
- Support for Oxygen v4 / Laravel v8
