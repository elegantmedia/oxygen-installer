# Oxygen Installer

Oxygen installer simply downloads the latest version of Laravel and automatically installs oxygen and sets it up as an oxygen project for you.

## Version Compatibility

| Oxygen Version   | Package Version | Branch       |
|------------------|-----------------|--------------|
| v7 / Laravel v10 | 2.2             | master       |
| v6 / Laravel v9  | 2.1             | master       |
| v5 / Laravel v8  | 2.0             | master       |
| v4 / Laravel v7  | 1.x             | version/v1.x |

See [change log for change history](CHANGELOG.md) and compatibility with past versions.

## Getting Started

### Installation

- To get started, first download the Oxygen Installer using Composer. 
- If you already have it installed, run it again to update the version. The latest version is `2.2.0`

```
composer global require emedia/oxygen-installer
```

Make sure to place Composer's system-wide vendor bin directory in your `$PATH` so the executables can be located by your system. This directory exists in different locations based on your operating system; however, some common locations include:

```
macOS: $HOME/.composer/vendor/bin
Windows: %USERPROFILE%\AppData\Roaming\Composer\vendor\bin
GNU / Linux Distributions: $HOME/.config/composer/vendor/bin or $HOME/.composer/vendor/bin
```

You could also find the composer's global installation path by running `composer global about` and looking up from the first line.

After a successful installation, you should get the latest version number by running `oxygen -v`.

### How to Use

Once installed, the `oxygen new` command will create a fresh Laravel + Oxygen installation in the directory you specify. For instance, `oxygen new blog` will create a directory named `blog` containing a fresh Laravel + Oxygen installation with all dependencies already installed:

To create a new Oxygen project, run 
```
oxygen new myproject
```

You can also pass the common parameters with just one line and go for a coffee, while it does the complete setup.
```
oxygen new myproject --name 'My Project' --dev_url 'localhost.test' --dbname mydatabase --dbuser john --dbpass mypassword --mailuser smtp_username --mailpass smtp_password
```

### Change Oxygen/Laravel Version

By default, it will install the current version. You can switch the version if required.

```
// Install Laravel 10 + Oxygen 7 (default)
oxygen new myproject

// Install Laravel 9 + Oxygen 6
oxygen new myproject --o_version 6

// Install Laravel 8 + Oxygen 5
oxygen new myproject --o_version 5

// Install Laravel 7 + Oxygen 4
oxygen new myproject --o_version 4

// Install dev branch
oxygen new project --o_version dev
```


### Available Options

The following options are available with the installer. All flags are optional, so you can set one or many.

| CLI Option  | What it Does                                        |
|-------------|-----------------------------------------------------|
| --o_version | Target version of Oxygen (4 to 7 or 'dev')          |
| --name      | Name of the project                                 |
| --email     | Admin user's email address (to seed the database)   |
| --dev_url   | VirtualHost's APP_URL                               |
| --force     | Forces install even if the directory already exists |
| --dbhost    | DB Host                                             |
| --dbport    | DB Port                                             |
| --dbname    | DB Name                                             |
| --dbuser    | DB User                                             |
| --dbpass    | DB Pass                                             |
| --mailhost  | Mail Host                                           |
| --mailport  | Mail Port                                           |
| --mailuser  | Mail Username                                       |
| --mailpass  | Mail Password                                       |
| --no-npm    | Don't install NPM packages                          |


## Contributing and Developer Instructions

Please see [CONTRIBUTING](CONTRIBUTING.md) and for details.

## Copyright

Copyright (c) 2023 Elegant Media.
