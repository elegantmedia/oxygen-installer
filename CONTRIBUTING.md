# Contributing

## Pull Requests

- **[PSR-2 Coding Standard](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md)** - Check the code style with `composer check-style` and fix it with `composer fix-style`.

- **Create feature branches** - Don't ask us to pull from your master branch.

- **One pull request per feature** - If you want to do more than one thing, send multiple pull requests.

- **Send coherent history** - Make sure each individual commit in your pull request is meaningful. If you had to make multiple intermediate commits while developing, please [squash them](http://www.git-scm.com/book/en/v2/Git-Tools-Rewriting-History#Changing-Multiple-Commit-Messages) before submitting.


## Development Setup

1. Clone the project to your local machine. Get the path to that folder, for example `/Users/john/projects/oxygen-installer`
2. Create a (separate) new folder to test, and name it `installer_test`. Go to that folder in CLI. In this example, it will be `/Users/john/projects/installer_test`.
3. From the folder at Step #2, you'll be able to call the installer directly. For example.

```
php /Users/shane/www/sites-laravel/EMedia/oxygen-installer/bin/oxygen
```
