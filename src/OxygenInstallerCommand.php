<?php

namespace EMedia\Oxygen\Installer\Console;

use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class OxygenInstallerCommand extends Command
{

	use InstallsPackages;

	private $o_version;
	private $directory;
	private $startTime;

	/**
	 * Configure the command options.
	 *
	 * @return void
	 */
	protected function configure()
	{
		// primary settings
		$this->setName('new')
			->setDescription('Create a new Oxygen application')
			->addArgument('folder', InputArgument::OPTIONAL, 'Name of the directory')
			->addOption('force', 'f', InputOption::VALUE_NONE, 'Forces install even if the directory already exists')
		;

		// installer settings
		$this->addOption('no-npm', null, InputOption::VALUE_NONE, "Don't install NPM dependencies?");

		// oxygen settings
		$this
			->addOption('o_version', null, InputOption::VALUE_REQUIRED, 'Target Oxygen Version', 7)

			->addOption('name', null, InputOption::VALUE_REQUIRED, 'Name of the project')
			->addOption('dev_url', null, InputOption::VALUE_OPTIONAL, "VirtualHost's APP_URL")
			->addOption('email', null, InputOption::VALUE_OPTIONAL, "Admin user's email address")

			->addOption('dbconn', null, InputOption::VALUE_OPTIONAL, "Database connection")
			->addOption('dbhost', null, InputOption::VALUE_OPTIONAL, "Database host")
			->addOption('dbport', null, InputOption::VALUE_OPTIONAL, "Database port")
			->addOption('dbname', null, InputOption::VALUE_OPTIONAL, "Database name")
			->addOption('dbuser', null, InputOption::VALUE_OPTIONAL, "Database user")
			->addOption('dbpass', null, InputOption::VALUE_OPTIONAL, "Database pass")

			->addOption('mailer', null, InputOption::VALUE_OPTIONAL, "Mail driver")
			->addOption('mailhost', null, InputOption::VALUE_OPTIONAL, "Mail host", "0.0.0.0")
			->addOption('mailport', null, InputOption::VALUE_OPTIONAL, "Mail port", "1025")
			->addOption('mailuser', null, InputOption::VALUE_OPTIONAL, "Mail user")
			->addOption('mailpass', null, InputOption::VALUE_OPTIONAL, "Mail port")
			->addOption('mailenc', null, InputOption::VALUE_OPTIONAL, "Mail encryption")

			->addOption('public_folder', null, InputOption::VALUE_OPTIONAL, "Public folder name", 'public_html')
		;
	}

	/**
	 *
	 * Start the command
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 *
	 * @return int
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->startTime = microtime(true);

		// install laravel project
		// the `folder` is renamed to `name` so it doesn't conflict with
		// laravel installer's `name`, which actually refers to the folder
		$name = $input->getArgument('folder');

		// get the full installation path
		$this->directory = $name && $name !== '.' ? getcwd() . '/' . $name : getcwd();

		// get the target Oxygen version
		$this->o_version = $input->getOption('o_version');
		if (!in_array($this->o_version, ['4', '5', '6', '7', 'dev'], false)) {
			throw new RuntimeException("Version {$this->o_version} is invalid.");
		}

		if (!$input->getOption('force')) {
			$this->verifyApplicationDoesntExist($this->directory);
		}

		if ($input->getOption('force') && $this->directory === '.') {
			throw new RuntimeException('Cannot use --force option when using current directory for installation!');
		}

		// check required software exists
		$requiredCommands = [
			'git --version'     => 'GIT',
			'node --version'    => 'Node.js',
			'npm --version'     => 'NPM (Node Package Manager)',
		];
		$this->verifyRequiredCommandsExist($requiredCommands, $input, $output);

		// install laravel project
		$this->installLaravelProject($input, $output, $name);

		// update composer json with private repositories and other custom values
		$this->updateComposerJson($this->directory);

		// install oxygen
		$this->installOxygen($input, $output);

		return 0;
	}

	/**
	 *
	 * Create the laravel project
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @param $name
	 *
	 */
	protected function installLaravelProject(InputInterface $input, OutputInterface $output, $name)
	{
		$composer 	= $this->findComposer();
		$directory 	= $this->directory;

		switch ($this->o_version) {
			case 'dev':
				$laravelVersion = 'dev-master';
				break;
			case '4':
				$laravelVersion = '7.*';
				break;
			case '5':
				$laravelVersion = '8.*';
				break;
			case '6':
				$laravelVersion = '9.*';
				break;
			case '7':
			default:
				$laravelVersion = '10.*';
		}

		$commands = [
			"$composer create-project laravel/laravel" .
			"  --prefer-dist --remove-vcs \"$directory\" $laravelVersion",
		];

		if ($directory !== '.' && $input->getOption('force')) {
			if (PHP_OS_FAMILY === 'Windows') {
				array_unshift($commands, "rd /s /q \"$directory\"");
			} else {
				array_unshift($commands, "rm -rf \"$directory\"");
			}
		}

		if (PHP_OS_FAMILY !== 'Windows') {
			$commands[] = "chmod 755 \"$directory/artisan\"";
		}

		if (($process = $this->runCommands($commands, $input, $output))->isSuccessful()) {
			$output->writeln("<comment>Laravel `v{$laravelVersion}` Installed.</comment>");

			return $process->getExitCode();
		}

		$output->writeln('Failed to run commands');
		foreach ($commands as $line) {
			$output->writeln($line);
		}

		throw new RuntimeException('Failed to create a new Laravel Project. This is not an issue with Oxygen.');
	}



	/**
	 *
	 * Get oxygen packages repository list.
	 *
	 * @return array
	 */
	protected function getNewComposerData()
	{
		switch ($this->o_version) {
			case '4':
				$fileName = 'oxygen-composer-v4.json';
				break;
			case '5':
				$fileName = 'oxygen-composer-v5.json';
				break;
			case '6':
				$fileName = 'oxygen-composer-v6.json';
				break;
			case '7':
			default:
				$fileName = 'oxygen-composer-v7.json';
		}

		return json_decode(file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . $fileName), true);
	}

	/**
	 *
	 * Install Oxygen
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 */
	protected function installOxygen(InputInterface $input, OutputInterface $output)
	{
		$composer = $this->findComposer();

		chdir($this->directory);

		if ($this->o_version === 'dev') {
			$oxygenVersion = 'dev-master';
		} else {
			$oxygenVersion = "^{$this->o_version}.0";
		}

		$commands = [
			$composer . " require emedia/oxygen:\"{$oxygenVersion}\"",
			'git init',
			'git add -A',
			'git commit -m "Initial Commit."',
			$this->getOxygenSetupCommand($input),
			$composer . ' dump-autoload',

			// TODO: migrate and seed the database - won't work without setting the database first
			// 'php artisan db:refresh',
		];

		// build the front-end
		if (!$input->getOption('no-npm')) {
			$commands = array_merge($commands, [
				'npm install',
				'npm run dev',
				'npm run dev',
			]);
		}

		$process = $this->runCommands($commands, $input, $output);

		// show running time
		$duration = microtime(true) - $this->startTime;
		$mins = (int)floor($duration / 60);
		$seconds = (int)$duration % 60;
		$output->writeln("Completed in {$mins} minutes {$seconds} seconds.");

		if ($process->isSuccessful()) {
			$output->writeln("<comment>See README.md for instructions. Don't forget to commit.</comment>");
			$output->writeln('<comment>Project ready to go! 😸</comment>');
		}
	}


	/**
	 *
	 * Build the command string for oxygen's setup
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 *
	 * @return string
	 */
	protected function getOxygenSetupCommand(InputInterface $input)
	{
		// filter params we're going to pass to oxygen's setup command
		$oxygenArgs = array_filter($input->getOptions(), function ($key) {
			return in_array($key, [
				'name', 'dev_url', 'email',
				'dbconn', 'dbhost', 'dbport', 'dbname', 'dbuser', 'dbpass',
				'mailer', 'mailhost', 'mailport', 'mailuser', 'mailpass', 'mailenc',
				'public_folder',
			]);
		}, ARRAY_FILTER_USE_KEY);

		// build oxygen's setup command string
		$oxygenSetupCommand = [PHP_BINARY . ' artisan oxygen:dashboard:install'];

		switch ($this->o_version) {
			case '4':
				$oxygenArgs['devurl'] = $oxygenArgs['dev_url'];
				$oxygenSetupCommand = [PHP_BINARY . ' artisan setup:oxygen-project'];
				break;
			case '5':
			default:
				// use defaults for latest version
		}

		foreach ($oxygenArgs as $key => $value) {
			if (!empty($value)) {
				$oxygenSetupCommand[] = '--' . $key . ' ' . escapeshellarg($value);
			}
		}

		return implode(' ', $oxygenSetupCommand);
	}
}
