<?php

namespace EMedia\Oxygen\Installer\Console;

use RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

trait InstallsPackages
{

	/**
	 *
	 * Verify pre-requisite software exists
	 *
	 * @param $list
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 *
	 * @return bool
	 */
	protected function verifyRequiredCommandsExist($list, InputInterface $input, OutputInterface $output): bool
	{
		foreach ($list as $command => $name) {
			$process = Process::fromShellCommandline($command);
			$process->run();

			if (!$process->isSuccessful()) {
				$output->write($process->getErrorOutput());
				throw new RuntimeException(
					"{$name} not found. This is required to proceed. Check by typing `{$command}` and press enter."
				);
			}
		}

		return true;
	}

	/**
	 *
	 * Append oxygen packages into the project composer.json file.
	 *
	 * @param string $directory
	 *
	 * @return $this
	 *
	 */
	protected function updateComposerJson($appDirectory)
	{
		$composerFile = $appDirectory . DIRECTORY_SEPARATOR . 'composer.json';

		$composerData = json_decode(file_get_contents($composerFile), true);

		$composerData = array_merge($composerData, $this->getNewComposerData());

		$jsonData = json_encode($composerData, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);

		file_put_contents($composerFile, $jsonData);

		return $this;
	}


	/**
	 * Verify that the application does not already exist.
	 *
	 * @param string $directory
	 *
	 * @return void
	 */
	protected function verifyApplicationDoesntExist($directory)
	{
		if ((is_dir($directory) || is_file($directory)) && $directory != getcwd()) {
			throw new RuntimeException('Application already exists!');
		}
	}

	/**
	 *
	 * Get the composer command for the environment.
	 *
	 * @return string
	 */
	protected function findComposer()
	{
		$composerPath = getcwd() . '/composer.phar';

		if (file_exists($composerPath)) {
			return '"' . PHP_BINARY . '" ' . $composerPath;
		}

		return 'composer';
	}

	/**
	 * Run the given commands.
	 *
	 * @param array $commands
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 *
	 * @return Process
	 */
	protected function runCommands($commands, InputInterface $input, OutputInterface $output)
	{
		if ($input->getOption('no-ansi')) {
			$commands = array_map(function ($value) {
				if (substr($value, 0, 5) === 'chmod') {
					return $value;
				}

				return $value . ' --no-ansi';
			}, $commands);
		}

		if ($input->getOption('quiet')) {
			$commands = array_map(function ($value) {
				if (substr($value, 0, 5) === 'chmod') {
					return $value;
				}

				return $value . ' --quiet';
			}, $commands);
		}

		$process = Process::fromShellCommandline(implode(' && ', $commands), null, null, null, null);

		if ('\\' !== DIRECTORY_SEPARATOR && file_exists('/dev/tty') && is_readable('/dev/tty')) {
			try {
				$process->setTty(true);
			} catch (RuntimeException $e) {
				$output->writeln('Warning: ' . $e->getMessage());
			}
		}

		$process->run(function ($type, $line) use ($output) {
			$output->write('    ' . $line);
		});

		return $process;
	}

}
